from pathlib import Path

file = Path(input("Veuillez choisir un path de fichier : "))

try:
    print(file.read_text(encoding="utf-8"))
except FileNotFoundError as e:
    print("Le chemin n'est pas correct : ",e)
except UnicodeDecodeError as f:
    print("Impossible de lire le fichier : ",f) 
finally:
    print("Fin du script ;)")
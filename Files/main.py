import json
from pathlib import Path
chemin = r'C:\Users\ThomasSAVATON\OneDrive - FIT Retail\Documents\Perso\CoursPythonGit\CoursPythonAPI\Files\parameter.json'

# with open(chemin, "r", encoding='utf-8') as f:
#     #json.dump(list(range(10)), f, indent=4)
#     liste = json.load(f)
#     print(liste)
    
a = Path.cwd()

print(a.is_file())
print(a.is_dir())
print(a.parent)
print(a.suffix)

test = a / "Test" / "test.txt"
test.touch()

test.write_text("De De DEBOUT !\nVous m'avez manqué !")
print(test.read_text())
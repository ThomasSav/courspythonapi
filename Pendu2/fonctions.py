import pickle
import random
import Pendu2.donnees as d



def menu():
    print("Bienvenue sur notre super jeu du pendu !!")
    choix = 0
    while choix != 3:
        choix = input("Veuillez choisir : 1 pour jouer, 2 pour consulter les scores et 3 pour quitter")
        try:
            choix = int(choix)
            assert choix == 1 or choix == 2 or choix == 3
        except ValueError:
            print("Veuillez saisir un chiffre entre 1 et 3")
            continue
        except AssertionError:
            print("Veuillez saisir un chiffre entre 1 et 3")
            continue
        if choix == 1:
            jeu()
        elif choix == 2:
            readScore()
    print("Merci d'avoir joué :) !")
def jeu():
    alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    hasard = random.randint(0, len(d.mots)-1)
    guessWordClear = [char for char in d.mots[hasard]]
    guessWordHidden = ["*" for char in guessWordClear]
    while d.chance > 0:
        print("Voici le mot à deviner : {}".format(guessWordHidden))
        print("Il vous reste {} chance(s)".format(d.chance))
        lettreChoisi = input("Veuillez choisir une lettre : ")
        lettreChoisi = lettreChoisi.lower()
        if lettreChoisi in alphabet:
            if lettreChoisi in guessWordClear:
                for value in guessWordClear:
                    if lettreChoisi == value:
                        i = guessWordClear.index(value)
                        guessWordHidden[i] = guessWordClear[i]
                        guessWordClear[i] = "8"
            else:
                print("La lettre {} ne figure pas dans ce mot".format(lettreChoisi))
                d.chance -= 1
        else:
            print("Vous n'avez pas choisi une lettre, dommage")
            d.chance -= 1
            continue

        if "*" not in guessWordHidden:
            break

    if(d.chance > 0):
        print("Félicitation vous avez trouvez le mot : {}, il vous reste {} vie".format(guessWordHidden, d.chance))
        player = input("Veuillez saisir un nom de joueur pour le livre des records : ")
        writeScore(player, d.chance)
        d.chance = 8
    else:
        print("Dommage le mot a deviné était {}".format(d.mots[hasard]))
        d.chance = 8


def readScore():
    with open("score.txt", "rb") as file:
        monUnpicker = pickle.Unpickler(file)
        try:
            score = monUnpicker.load()
        except EOFError:
            print("Aucun score n'est disponible")
            return
        print("Voici les scores :")
        for i, a in score.items():
            print("{} = {}".format(i, a))

def writeScore(a, j):
    score = {}
    with open("score.txt", "rb") as file:
        monUnpicker = pickle.Unpickler(file)
        try:
            score = monUnpicker.load()
        except EOFError:
            print("Pas de score disponible")
    with open("score.txt", "wb") as file:
        monPicker = pickle.Pickler(file)
        score[a.lower()] = j
        score = monPicker.dump(score)

if __name__ == "__main__":
    menu()
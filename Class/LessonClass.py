class Personne:
    """Classe définissant une personne par son :
    -nom
    -prénom
    -âge
    -adresse"""

    def __init__(self, nom, prenom):

        self.nom = nom
        self.prenom = prenom
        self.age = 25
        self.adresse = "23 boulevard du Cateau, 59100 Roubaix"

bernard = Personne("Dupont", "Bernard")

print(bernard.nom)
print(bernard.prenom)
print(bernard.age)
print(bernard.adresse)


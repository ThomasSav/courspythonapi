class TableauNoir:
    """Classe définissant une surface sur laquelle on peut écrire,
    que l'on peut lire et effacer, par jeu de méthodes. L'attribut modifié
    est 'surface'"""

    def __init__(self):
        self.surface = ""

    def ecrire(self, messageAEcrire):

        if self.surface != "":
            self.surface += "\n"
        
        self.surface += messageAEcrire

tab = TableauNoir()

tab.ecrire("Je m'appel")
tab.ecrire("Thomas")

print(tab.surface)
          
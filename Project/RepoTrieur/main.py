from pathlib import Path

a = Path(r"C:\Users\ThomasSAVATON\Downloads")

dirs = {
    ".msi": "Executables",
    ".exe": "Executables",
    ".jpeg": "Images",
    ".jpg": "Images",
    ".png": "Images",
    ".pdf": "Documents",
    ".docx": "Documents"
        }

files = [f for f in a.iterdir() if f.is_file()]

for f in files:
    output_dir = a / dirs.get(f.suffix, "Autres")
    output_dir.mkdir(exist_ok=True)
    f.rename(output_dir / f.name )

import json, os
chemin = r'C:\Users\ThomasSAVATON\OneDrive - FIT Retail\Documents\Perso\CoursPythonGit\CoursPythonAPI\Project\Courses\BuyingList.json'

print("Bienvenue dans la superbe liste de course !")

choixUser = 0
if os.path.exists(chemin):
    with open(chemin, "r", encoding='utf-8') as f:
        listeDeCourse = json.load(f)
else :
    listeDeCourse = []

while choixUser != 5 :

    print("Choisissez une options parmis les 5 :")
    print("1: Ajouter un élément à la liste")
    print("2: Retirer un élément à la liste")
    print("3: Afficher la liste")
    print("4: Vider la liste")
    print("5: Quitter")

    choixUser = input("Votre choix ici : ")
    if choixUser.isdigit():
        choixUser = int(choixUser)
        if choixUser == 1:

            element = input("Que voulez vous ajouter dans votre liste ?")
            listeDeCourse.append(element)
            print(f"\"{element}\" a bien été ajouté à votre liste de course.")

        elif choixUser == 2:

            element = input("Que voulez vous retirer de votre liste : ")
            if element in listeDeCourse:
                listeDeCourse.remove(element)
                print(f"\"{element}\" a bien été retiré de votre liste de course (y) !")
            else:
                print(f"\"{element}\" n'est pas présent dans votre liste de course :/ ")

        elif choixUser == 3:

            if len(listeDeCourse) == 0:
                print("La liste de course est vide !")
            else:
                for i in listeDeCourse:
                    print(f"{listeDeCourse.index(i) + 1} : {i}")
        
        elif choixUser == 4:

            listeDeCourse.clear()
            print("La liste de course est maintenant vide !")
        
        elif choixUser == 5:
            break

        else:
            print("Veuillez choisir une option valdie :'( ")
    else:
        print("Veuillez choisir une option valdie :'( ")
    print("************************************************************************************")
print("Nous sauvegardons votre liste pour une utilisation antérieur !")
with open(chemin, "w", encoding='utf-8') as f:
  json.dump(listeDeCourse, f, indent=4)
print("Merci d'avoir utiliser l'application liste de course, à bientôt ! ")



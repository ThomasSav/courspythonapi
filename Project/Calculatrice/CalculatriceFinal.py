import random
import os

print("*****************************************")
print("Bienvenue dans la super calculatrice !!")
print("*****************************************")

a = input("Veuillez saisir un premier nombre : ")
b = input("Veuillez saisir un deuxième nombre : ")

if not a.isdigit() or not b.isdigit():
    print("Veuillez saisir deux nombres valides !")
else:
    print(f"L'addition de {a} et {b} est égale à {int(a)+int(b)}")
from pathlib import Path

a = Path(r"C:\Users\ThomasSAVATON\OneDrive - FIT Retail\Documents\Perso\CoursPythonGit\CoursPythonAPI\Project\Trieur\prenom.txt")

prenoms = a.read_text(encoding="utf-8")
prenomsList = prenoms.replace("\n", " ").replace("  ", " ").split(" ")
properPrenomList = [f.strip(",").strip(".").strip(" ").strip("\n") for f in prenomsList]
properPrenomList.sort()  
b = a.parent / "properPrenoms.txt"
b.touch()
b.write_text("\n".join(properPrenomList), encoding="utf-8")
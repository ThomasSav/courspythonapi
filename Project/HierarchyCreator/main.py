from pathlib import Path

a = Path(r"C:\Users\ThomasSAVATON\OneDrive - FIT Retail\Documents\Perso\CoursPythonGit\CoursPythonAPI\HierarchyCreator")

d = {"Films": ["Le seigneur des anneaux",
               "Harry Potter",
               "Moon",
               "Forrest Gump"],
     "Employes": ["Paul",
                  "Pierre",
                  "Marie"],
     "Exercices": ["les_variables",
                   "les_fichiers",
                   "les_boucles"]}

for key, values in d.items():
    outputdir1 = a / key
    outputdir1.mkdir(exist_ok=True)
    for value in values:
        outputdir2 = a / key / value
        outputdir2.mkdir(exist_ok=True)

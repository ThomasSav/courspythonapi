import random
import sys

print("*_* Jeu du nombre mystère *_*")

nbMystere = random.randint(0, 100)

essai = 0
nbUser = 0

while essai != 5:
    print(f"Il vous reste {5-essai} pour deviner le nombre mystère !")
    nbUser = input("Veuillez saisir un nombre entre 0 et 100 pour deviner le nombre mystère : ")

    if nbUser.isdigit:
        nbUser = int(nbUser)

        if nbUser < nbMystere :
            print(f"Le nombre mystère est plus grand que {nbUser}")
            essai += 1
        elif nbUser > nbMystere :
            print(f"Le nombre mystère est plus petit que {nbUser}")
            essai += 1
        else:
            print(f"Félictiation vous avez trouver le nombre mystère {nbMystere} en {essai+1} tentatives ! ")
            sys.exit(0)
    else:
        print("Veuillez saisir un nombre valide")
print(f"Dommage, le nombre mystère était le {nbMystere} !")
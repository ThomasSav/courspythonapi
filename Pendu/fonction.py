import os
from donnee import *
import random
import pickle
pseudo = ''
choice = ''


def start():
    global pseudo
    print("***************************************************************************************************")
    print("Hello dear User, I hope you are fine and you are ready to have fun !")
    print("***************************************************************************************************")
    pseudo = input("please tell us your little surname :) : ")
    print("***************************************************************************************************")
    verifPseudo()
    menu()

def menu():
    menuChoice="0"
    while (menuChoice != "1" and menuChoice != "2" and menuChoice != "3") :
        print("1 => Jeu du pendu")
        print("2 => Consulter les scores")
        print("3 => Quitter le programme")
        print("***************************************************************************************************")
        menuChoice = input("Veuillez choisir ce que vous voulez faire :")
        print("***************************************************************************************************")

    if menuChoice == "1":
        jeu()
    elif menuChoice == "2":
        readScore()
    else:
        print("***************************************************************************************************")
        print("Merci d'avoir joue cya !")
        print("***************************************************************************************************")
        SystemExit()

def jeu():
    global chance
    global choice
    chance = 8
    lettreTest = list()
    select = random.randint(0, (len(mots)-1))

    motToGuess = list(mots[select])
    motToGuessHidden = list(mots[select])
    i = 0

    while i< len(motToGuess):

        motToGuessHidden[i] = "_"
        i += 1
    
    randomLetter = random.randint(0, len(motToGuess)-1)

    motToGuessHidden[randomLetter] = motToGuess[randomLetter]

    while "_" in motToGuessHidden and chance > 0:
        print("choix = ", choice)
        print("chance = ", chance)
        print("il vous reste  : ", chance, " chance de réussir, courage !")
        print("Voici la liste des lettres essayées : ", lettreTest)
        print(motToGuessHidden)
        choice = input("Veuillez choisir une lettre :")
        choice = verifLetter()
        if choice != "":
            if choice in motToGuess:
                for i, value in enumerate(motToGuess):
                    if value == choice:
                        motToGuessHidden[i] = motToGuess[i]
                
            else:
                print("Dommage la lettre n'est pas présente dans le mot !")
                chance -= 1
                lettreTest.append(choice)
        
    
    if chance > 0 :
        print("***************************************************************************************************")
        print("Félicitation vous avez trouver le mot mystère !")
        print("***************************************************************************************************")
        score = {"Pseudo": pseudo, "Score": chance, "Mot découvert": str(motToGuess)}
        os.chdir("C:/Users/t.savaton/Documents/CoursPython/Pendu")
        with open("score.txt", "ab") as fichier:
            monPickle = pickle.Pickler(fichier)
            monPickle.dump(score)
            monPickle.dump("\n")
    else:
        print("Dommage une prochaine fois surement :) !")
    
    menu()

def readScore():
    os.chdir("C:/Users/t.savaton/Documents/CoursPython/Pendu")
    with open("score.txt", "rb") as fichier:
        monUnpicker = pickle.Unpickler(fichier)
        listScore = monUnpicker.load()
        print(listScore)
    menu()


def verifPseudo():
    global pseudo 
    print("Pseudo : ", pseudo)
    if len(list(pseudo)) == 0 or len(list(pseudo)) > 10:
        pseudo = input("Your surname can't be longer than 10 characters and should have at least one, choose another one : ")
        verifPseudo()
    else:
        print("Welcome ", pseudo, ", let's go !")
    
def verifLetter():
    global choice
    global chance

    if type(choice) != str:
        print("Veuillez chosir un type alphanumérique !")
        return ""
    if len(list(choice)) != 1 or choice == "":
        print("Vous ne pouvez choisir qu'une lettre !")
        return ""
    choice = choice.casefold()
    return choice


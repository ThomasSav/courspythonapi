import os

def tableDe(a):
    print("Voici la table de ",a)
    i = 0
    while i <= 10:
        print("Le résultat de ",i,"*",a," : ",i*a)
        i += 1

if __name__ == "__main__":
    tableDe(6)
    os.system("pause")
    
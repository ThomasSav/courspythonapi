import os
from Calculatrice import *

annee = input('Veuillez saisir une année : ')
try:
    annee = int(annee)
    assert annee > 0
except ValueError:
    print("Vous n'avez pas saisi un nombre")
except AssertionError:
    print("Le nomble doit être supérieur à 0, réfléchis !")
    
if annee % 400 == 0 or (annee %4 == 0 and annee % 100 != 0):
    print("L'année saisie est bissextile")
else:
    print("L'année n'est pas bissextile.")

a = input("Veuillez saisir un entier : ")
a = int(a)

tableDe(a)

os.system("pause")

import os
"""
maListe = [1,2,3,4,5,6,7,8,9]

maListe2 = ['yolo', 'swag']
maListe.__delitem__(0)

print(maListe)

maListe.append(50)

print(maListe)

maListe.extend(maListe2)

print(maListe)

maListe.clear()

print(maListe)

maChaine = "yolo \nswag"

print(maChaine)

maListe3 = [5, 6, 7, 8]

print(maListe3)
maListe3.append(19)
print(maListe3)
maListe3.remove(19)
print(maListe3)

for i, elt in enumerate(maListe3):
    print("A l'index {} nous avons: {}.".format(i, elt))

#del maListe3
print(maListe3)

def afficherFloatant(a):
    if type(a) is not float:
       raise TypeError("Le paramètre attendu est un floatant")
    a = str(a)
    partieEntiere, partieFloat = a.split(".")
    return ".".join([partieEntiere, partieFloat[:3]])

a = 1.4161949841619
print(afficherFloatant(a))

def afficherChaine(*param, sep=" et ", fin="\n"):
    param = list(param)
    for i, elt in enumerate(param):
        param[i] = "Index: {}, Value: {}".format(i, elt)

    chaine = sep.join(param)

    chaine += fin

    print(chaine, end='')

afficherChaine("yolo", "swag", "michel", "denis", "JeanLouis", 59)

"""
    
listeFruit = [15, 2, 20, 5, 3]
qtRetire= 7
print([nbFruit - qtRetire for nbFruit in listeFruit if nbFruit > qtRetire])

inventaire = [
("pommes", 22),
("melons", 4),
("poires", 18),
("fraises", 76),
("prunes", 51),
]

print(inventaire)

print(sorted(inventaire, key=lambda inv: inv[1]))

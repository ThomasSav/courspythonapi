class TableauNoir:
    """Une classe qui agit comme un tableau"""

    def __init__(self):
        self.surface = ""

    def ecrire(self, message):
        """Methode permettant d'écrire sur le tableau"""
        if self.surface != "":
            self.surface += "\n"

        self.surface += message

    def effacer(self):
        """Methode permettant de remettre à 0 le tableau"""
        self.surface = ""

    def lire(self):
        """Methode permettant d'afficher le contenu du tableau"""
        print(self.surface)

if __name__ == "__main__":
    table = TableauNoir()
    table.ecrire("Yolo")
    table.ecrire("Swag")
    table.lire()
    help(TableauNoir)
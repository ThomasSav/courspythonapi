class Personne:
    """Classe définissant une personne caractérise par:
    - Nom
    - Prenom
    - Age
    - Lieu de résidence
    """
    def __init__(self, n, p, a, l):
        self.nom = n
        self.prenom = p
        self.age = a
        self.lieuResidence = l

    def monId(self):
        print("Je m'appelle {} {}, j'ai {}. J'habite actuellement : {}".format(self.nom, self.prenom, self.age, self.lieuResidence))
if __name__ == "__main__":

    bernard = Personne("Dupont", "Bernard", 45, "23 boulevard du cateau, 59100 Roubaix")
    bernard.monId()